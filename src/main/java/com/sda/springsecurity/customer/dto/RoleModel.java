package com.sda.springsecurity.customer.dto;

import java.util.Objects;

public class RoleModel {
    private Long id;
    private String name;

    public RoleModel(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        RoleModel roleModel = (RoleModel) o;
        return Objects.equals(id, roleModel.id) &&
                Objects.equals(name, roleModel.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
