package com.sda.springsecurity.customer.dto;

import com.sda.springsecurity.customer.constraint.FieldMatch;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@FieldMatch.List({
        @FieldMatch(
                first = "password",
                second = "confirmPassword",
                message = "The password fields must match")
})
public class CustomerRegistrationModel {
    @NotBlank
    private String login;
    @NotBlank
    private String password;
    @NotBlank
    private String confirmPassword;
    @Email
    @NotBlank
    private String email;
    @AssertTrue
    private Boolean terms;
    private Set<RoleModel> roles;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getTerms() {
        return terms;
    }

    public void setTerms(Boolean terms) {
        this.terms = terms;
    }

    public Set<RoleModel> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleModel> roles) {
        this.roles = roles;
    }
}
