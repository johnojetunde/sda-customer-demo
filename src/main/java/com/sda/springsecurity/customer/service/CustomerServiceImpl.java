package com.sda.springsecurity.customer.service;

import com.sda.springsecurity.customer.dto.CustomerRegistrationModel;
import com.sda.springsecurity.customer.dto.CustomerResponseModel;
import com.sda.springsecurity.customer.dto.RoleModel;
import com.sda.springsecurity.customer.entity.Customer;
import com.sda.springsecurity.customer.entity.Role;
import com.sda.springsecurity.customer.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository,
                               BCryptPasswordEncoder passwordEncoder) {
        this.customerRepository = customerRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Customer findByLogin(String login) {
        return customerRepository.findByLogin(login);
    }

    @Override
    public Customer save(CustomerRegistrationModel registrationModel) {
        Customer customer = new Customer();
        customer.setLogin(registrationModel.getLogin());
        customer.setEmail(registrationModel.getEmail());
        customer.setPassword(passwordEncoder.encode(registrationModel.getPassword()));
        customer.setRoles(remapRoles(registrationModel.getRoles()));
        return customerRepository.save(customer);
    }

    public CustomerResponseModel getCustomer(String login) {
        Customer customer = customerRepository.findByLogin(login);
        return customer.toModel();
    }

    private Collection<Role> remapRoles(Collection<RoleModel> roles) {
        return roles.stream()
                .map(Role::fromModel)
                .collect(toSet());
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Customer customer = customerRepository.findByLogin(login);
        if (customer == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new User(customer.getLogin(),
                customer.getPassword(),
                mapRolesToAuthorities(customer.getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(toList());
    }
}
