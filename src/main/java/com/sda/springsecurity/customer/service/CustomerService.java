package com.sda.springsecurity.customer.service;

import com.sda.springsecurity.customer.dto.CustomerRegistrationModel;
import com.sda.springsecurity.customer.entity.Customer;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface CustomerService extends UserDetailsService {
    Customer findByLogin(String login);

    Customer save(CustomerRegistrationModel registrationModel);
}
