package com.sda.springsecurity.customer.entity;

import com.sda.springsecurity.customer.dto.CustomerResponseModel;

import javax.persistence.*;
import java.util.Collection;

import static java.util.stream.Collectors.toSet;

@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String login;
    @Column(unique = true, nullable = false, columnDefinition = "VARCHAR(250)")
    private String email;
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "customers_roles",
            joinColumns = @JoinColumn(
                    name = "customer_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"
            )
    )
    private Collection<Role> roles;

    public Customer() {
    }

    public Customer(String login, String email, String password) {
        this.login = login;
        this.email = email;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public CustomerResponseModel toModel() {
        CustomerResponseModel responseModel = new CustomerResponseModel();
        responseModel.setEmail(this.email);
        responseModel.setLogin(this.login);
        responseModel.setRoles(this.getRoles().stream()
                .map(Role::toModel)
                .collect(toSet())
        );

        return responseModel;
    }

    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles +
                '}';
    }
}
