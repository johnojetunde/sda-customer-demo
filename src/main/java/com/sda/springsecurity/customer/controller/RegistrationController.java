package com.sda.springsecurity.customer.controller;

import com.sda.springsecurity.customer.dto.CustomerRegistrationModel;
import com.sda.springsecurity.customer.entity.Customer;
import com.sda.springsecurity.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/registration")
public class RegistrationController {
    @Autowired
    private CustomerService customerService;

    @ModelAttribute("customer")
    public CustomerRegistrationModel customerRegistration() {
        return new CustomerRegistrationModel();
    }

    @GetMapping
    public String showRegistrationForm(Model model) {
        return "registration";
    }

    @PostMapping
    public String register(@ModelAttribute("customer") @Valid CustomerRegistrationModel registrationModel,
                           BindingResult result) {
        Customer existing = customerService.findByLogin(registrationModel.getLogin());
        if (existing != null) {
            result.rejectValue("login", null, "There is already an account registered with this login");
        }

        if (result.hasErrors()) {
            return "registration";
        }

        customerService.save(registrationModel);
        return "redirect:/registration?success";
    }

}
