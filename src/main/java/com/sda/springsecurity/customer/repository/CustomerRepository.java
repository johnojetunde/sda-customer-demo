package com.sda.springsecurity.customer.repository;

import com.sda.springsecurity.customer.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Customer findByLogin(String login);
}
